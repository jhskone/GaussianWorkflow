%chk=dum.chk
%mem=20GB
%nprocshared=14
#P GFINPUT IOP(6/7=3) opt freq pbe1pbe/6-31G** geom=cartesian guess=harris scf(direct,tight) scfcyc=250

--------------------------------------
[benzene] geometry from molec. crystal 
--------------------------------------

0, 1
C  0.000  1.396  0.000
C  1.209  0.698  0.000
C  1.209 -0.698  0.000
C  0.000 -1.396  0.000
C -1.209 -0.698  0.000
C -1.209  0.698  0.000
H  0.000  2.479  0.000
H  2.147  1.240  0.000
H  2.147 -1.240  0.000
H  0.000 -2.479  0.000
H -2.147 -1.240  0.000
H -2.147  1.240  0.000


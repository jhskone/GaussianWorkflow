#!/usr/bin/python

import sys
import os

#######
# the default walltime, ncpus, partition can be edited here.
#partition = "broadwl-lc"
partition = "sandyb"
walltime = "36"
ncpusd = 28
mem = ncpusd * 2140

##############################################
# Email Notification Settings                #
#                                            #
# Set "send_email" to 1 and edit your email  #
# address to enable email notification       #
#                                            #
# email_options:                             #
#   b: begin                                 #
#   e: end                                   #
#   a: aborted                               #
#   (bea: send email when a job begin, end,  #
#         or aborted)                        #
#                                            #

send_email = 0
email_address = "youremail@domain.com"
email_options = "bea"

#                                            #
# End of Email Notification Settings         #
##############################################


l = len(sys.argv)

if l < 2:
   print 'Usage: '+os.path.basename(sys.argv[0])+' filename.com ncpus(='+str(ncpusd)+') mem(='+str(mem)+') walltime(='+str(walltime)+') partition(='+partition+')'
   sys.exit(1)

filename = sys.argv[1]

# nproc:  # of cpus from .com files
# ncpus:  # of cpus from command line
# ncpusd: default # of cpus
nproc = 0
ncpus = 0

if not os.path.isfile(filename):
   print "Cannot find "+filename
   sys.exit(1)


basename = os.path.splitext(filename)[0]
ofname = basename+".out"
#sfname = basename+".info"
qfname = basename+".sbatch"
output = open(qfname,'w')

if l >= 6:
   partition    = sys.argv[5]

if l >= 5:
   walltime = sys.argv[4]

if l >= 4:
   mem      = sys.argv[3]

if l >= 3:
   ncpus    = int(sys.argv[2])
   if l == 3:
      mem   = ncpus * 1000

# read ncpus from .com files 
input = open(filename,'r+')

while True:
   line = input.readline()

   # break if EOF or error or for blank line
   if not line:
      break

   if len(line) == 0:
      break

   line = line.strip()
   s = line.split('=')
   s0 = s[0].lower()
   if s0 == '%nprocshared' or s0 == '%nproc':
      nproc = int(s[1])
      print "ncpus read from input file. ncpu=%d." % nproc
      if ncpus != 0 and ncpus != nproc:
          print "warning: ncpus is different from nproc in input file"
      ncpus = nproc


# set ncpus to default if no nproc is read from .com files
if nproc == 0:
   if ncpus == 0:
      ncpus = ncpusd
   print "set ncpus=%d." % ncpus

#  update .com file
   input.seek(0)
   old = input.read()
   input.seek(0)
   input.write("%%nprocshared=%d\n" % ncpus)
   input.write(old)

input.close()

output.write("#!/bin/bash\n")
output.write("#SBATCH --nodes=1\n")
output.write("#SBATCH --ntasks-per-node="+str(ncpus)+"\n")
output.write("#SBATCH --partition="+str(partition)+"\n")
#output.write("#SBATCH --ntasks-per-node 28\n")
output.write("#SBATCH --account=rcc-staff\n")
output.write("#SBATCH --time="+str(walltime)+":00:00\n")
#output.write("#SBATCH --time="+str(walltime)+":00:00\n")

output.write("\n")

#output.write("set echo\n")
output.write("module load gaussian\n")

output.write("mkdir -p $SCRATCH/"+basename+"\n")
output.write("export GAUSS_SCRDIR=$SCRATCH/"+basename+"\n")
output.write("\n")
#output.write("setenv OMP_NUM_THREADS "+str(ncpus)+"\n")

#output.write("source $g09root/g09/bsd/g09.login\n")

#output.write("cd $SLURM_SUBMIT_DIR\n")

#output.write("numactl -N +0 -m +0 g09 < "+filename+"  >& "+ofname+"\n")
output.write("g09 < "+filename+"  >& "+ofname+"\n")

output.write("# COPY OUTPUT \n")
#output.write("cp $SCRATCH/"+basename+"/*.out .\n")
#output.write("cp $SCRATCH/"+basename+"/*.chk .\n")
output.write("\n")
output.write("# CLEANING UP SCRATCH \n")
#output.write("rm -rf $SCRATCH/"+basename+" \n")
output.write("\n")
output.close()

os.system("sbatch "+qfname)


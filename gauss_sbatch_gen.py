#!/usr/bin/env python
#
########################################
# GAUSSIAN SBATCH FILE CREATION SCRIPT #
########################################
#  
import os, re, time, sys
import subprocess as sp
from optparse import OptionParser
#
# DEFINE FUNCTIONS
#
  
def ParseInput(ArgsIn):
   UseMsg='''
   Parse the passed input arguments for the Gaussian sbatch creation script'''
   parser=OptionParser(usage=UseMsg)
   parser.add_option('-o','--output',dest='output',type='str',help='This is the output file name. Default = infile -.in/inp/xyz +.out')
   parser.add_option('-e','--error',dest='error',type='str',help='This is the error file name. Default = infile -.in/inp/xyz +.err')
   parser.add_option('-d','--dryrun',dest='dryrun',action="store_true",default=False,help='Just creates the script file rather than running it')
   parser.add_option('--exclusive',dest='exclusive',action="store_true",default=False,help='When specified, will use all memory and all cores on each node requested')
   parser.add_option('-s','--scratch',dest='scratch',type='str',help='Use this scratch on the remote machine. Default = infile with the submission time appended to the end')
   parser.add_option('--name',dest='name',type='str',help='Specifies the name of the script file. Default = infile.sh')
   parser.add_option('-l','--time',dest='walltime',help="Specifies walltime. Default = 1 day. Use is '-l time=24:00:00'")
   parser.add_option('-p','--partition',dest='partition',type='str',help="Partition used") 
   parser.add_option('-m','--memory',type='str',default='',help="Memory per node. No default")
   parser.add_option('-N','--nodes',dest='nodes',type='int',help="Nodes used") 
   parser.add_option('-n','--ntasks',dest='ntasks',type='int',help="Number of cores to reserve for the job. Default = 1")
   parser.add_option('--ntasks-per-node',dest='ntpn',type='int',help="Number of tasks per node. Default = 1")
   parser.add_option('-q','--qos',dest='qos',type='str',default='',help="qos- defaults to gagalli-small")
   parser.add_option('--savefiles',action='append',help="Copy these files from the job's scratch directory to the local scratchdirectory",default=[])
   parser.add_option('--sendfiles',action='append',default=[],help="Copy these files from the input directory to the scratch directory")
   options,args=parser.parse_args(ArgsIn)
  
   if len(args)<2:
      print("Input file must be specified.")
      parser.print_help()
      sys.exit(1)
   else:
      return options,args

def HelpMsg():
   '''Print out help document for using this script '''
   
#
# BEGIN SCRIPT  
#
options,args=ParseInput(sys.argv)
if options.nodes:
        if (options.ntpn) or (options.exclusive):
                procs=options.nodes*options.ntpn
                print("Running on ", procs," processors")
                 
        else:
                print("WARNING: You are requesting entire node(s) but not using all cores on a node")
else:
        procs=1
  
for infile in args[1:]:
   if not os.path.exists(infile):
      print("Input file "+infile+" doesn't exist.")
      continue
  
   scriptfile = infile+'.sh'
   theTime = time.strftime("%Y%m%d%H%M%S")
   scriptfile=scriptfile+theTime
   strSLURM = ''
  
   if options.name:
      strSLURM += '#SBATCH --job-name='+str(options.name)+'\n'
   else:
      strSLURM += '#SBATCH --job-name='+str(scriptfile)+'\n'
  
   if options.walltime:
      strSLURM += '#SBATCH --'+str(options.walltime)+'\n'
   else:
      strSLURM += '#SBATCH --time=24:00:00\n'
  
   user = os.path.expandvars('$USER')
   if options.partition:
      strSLURM += '#SBATCH --partition='+str(options.partition)+'\n'
   else:
      strSLURM += '#SBATCH --partition=broadwl-lc\n'
   if options.nodes:
      strSLURM += '#SBATCH --nodes='+str(options.nodes)+'\n'
   else:
      strSLURM += '#SBATCH --nodes=1\n'
  
   if (options.ntpn):
      strSLURM += '#SBATCH --ntasks-per-node='+str(options.ntpn)+'\n'
   else:
      options.ntpn = 1
      strSLURM += '#SBATCH --ntasks-per-node=1\n'
  
   if options.memory:
      strSLURM += '#SBATCH --mem='+str(options.memory)+'\n'
  
   if options.qos:
      strSLURM += '#SBATCH --qos='+str(options.qos)+'\n'
  
   if options.output:
      outfile = options.output
   else:
      outfile = infile.replace('.in','').replace('.inp','').replace('.com','')+'.out'
  
   if options.error:
      errorfile = options.error
   else:
      errorfile = infile.replace('.in','').replace('.inp','').replace('.com','')+'.err'
  
   strSLURM += '#SBATCH --output='+errorfile+'\n'
   strSLURM += '#SBATCH --error='+errorfile+'\n'
   if options.scratch:
      scratch = options.scratch
   else:
      scratch = infile.replace('.in','').replace('.inp','').replace('.com','')
  
   user = os.path.expandvars('$USER')
   localCWD=os.popen('pwd').read().splitlines()[0]+'/'
   GAUSS_SCRATCH = os.path.expandvars('$SCRATCH')+'/'+infile.replace('.in','').replace('.inp','').replace('.com','')+theTime
   print("GAUSS SCR IS"+GAUSS_SCRATCH)
   sf = open(scriptfile,'w')
   redirectionOp = ' >& '
   redirectionOp2 = ' 2> '
   sf.write('#!/bin/bash\n')
   sf.write(strSLURM)
   sf.write('\n')
   sf.write('\n')
   sf.write('export OMP_NUM_THREADS=1\n')
   sf.write('export GAUSS_SCRDIR='+ GAUSS_SCRATCH + '\n')
   sf.write('if [ ! -e ' + GAUSS_SCRATCH +' ]\n')
   sf.write('then\n')
   sf.write('   mkdir ' + GAUSS_SCRATCH + '\n')
   sf.write('fi\n')
   theTime = time.strftime("%Y%m%d%H%M%S")
   sf.write('\n')
#   sf.write('cd ' +  GAUSS_SCRATCH + '\n')
   sf.write('\n')
   scratch += theTime
#   sf.write('mkdir ' + scratch + '\n')
#   sf.write('cd ' + scratch + '\n')
  
#   sf.write('cp '+localCWD+'/'+infile+' '+infile.replace(".in",theTime+".in")+'\n')
   for fl in options.sendfiles:
    if ':' in fl:
        (fl1,fl2)=fl.split(':')
        fl2=localCWD+'/'+fl2
    else:
        fl1=fl
        fl2=localCWD
    sf.write('cp -R '+localCWD+'/'+fl1+' $GLSCRATCH/'+scratch+'/'+'\n')
  
#   jobstr =' g09 < '+infile.replace(".in",theTime+".in") +' '+redirectionOp+localCWD+outfile+' '+redirectionOp2+errorfile+theTime
   jobstr =' g09 < '+infile.replace(".in",theTime+".in") +' '+redirectionOp+localCWD+outfile
   sf.write(jobstr+'\n')
   for fl in options.savefiles:
         if ':' in fl:
            (fl1,fl2)=fl.split(':')
            fl2=localCWD+'/'+fl2
         else:
            fl1=fl
            fl2=localCWD
         sf.write('cp -R '+'$GLSCRATCH/'+scratch+'/'+fl1+' '+fl2+'\n')
#   sf.write('cat '+errorfile+theTime+' >> '+localCWD+'/'+errorfile+'\n')
   sf.close()
#   if not options.dryrun:
#      sp.call(['sbatch', scriptfile])

#!/usr/bin/env python
#
############################
# WORKFLOW CREATION SCRIPT #
############################
#  
import os, re, time, sys
import subprocess as sp
from optparse import OptionParser
#
# SCRIPT INTENT:
#
# Create folder structure for each system and run Gaussian job workflow.

# Script will require name of system which can
# be passed to workflow script from a .com or .xyz file containing the cartesian
# coordinates of the system. 

# The workflow script should operate in three modes.
# Initialization mode:
# -- sets up the file directory tree structure and creates the inital run sbatch 
# and .com files and submits them to the scheduler. 
# Production mode:
# This is the automation of the rest of the workflow. It is to be executed after
# the first low level theory and basis set job launched by the initialization 
# mode successfully completes. 
# The production level submission script should create the inputs and sbatch files
# for each level of theory/basis set combination and check whether a particular
# run needs to be restarted if the job does not complete by the max wall time.
# Extraction mode: 
# This mode should work to extract/parse the log files and put the relevant 
# output into preformatted tables that can easily be plotted or included in 
# publication.